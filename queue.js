let collection = [];

// Write the queue functions below.
function print(){
	return collection;
}


function enqueue(name){
	collection[collection.length] = name;
	return collection;
}


function dequeue(){
	for (let i = 1; i < collection.length; i++){
		collection[i-1] = collection[i];
		collection.length--;
		return collection;
	}
}

function front(){
	let firstElement = collection[0];
	return firstElement;
}


function size(){
	return collection.length
}


function isEmpty(){
	if(collection !== null){
		return false;
	}
	else {

		return true;
	}
}

// Export create queue functions below.
module.exports = {
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};
